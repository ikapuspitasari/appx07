import android.app.AlertDialog
import android.content.Intent
import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.database.Cursor
import android.view.View
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import android.widget.CursorAdapter
import kotlinx.android.synthetic.main.activity_main.*
import android.content.DialogInterface
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import sari.ika.app7.DBOpenHelper
import sari.ika.app7.R
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var lsAdapter: ListAdapter
    lateinit var db: SQLiteDatabase
    lateinit var intentIntegrator: IntentIntegrator
    lateinit var dialog: AlertDialog.Builder
    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.btnscanQR -> {
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQR -> {
                val barCodeEnCoder = BarcodeEncoder()
                val bitmap = barCodeEnCoder.encodeBitmap(
                    txInput.text.toString(),
                    BarcodeFormat.QR_CODE, 400, 400
                )
                imV.setImageBitmap(bitmap)
            }
            R.id.btnSave -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentResult != null) {
            if (intentResult.contents != null) {
                txInput.setText(intentResult.contents)
                val strToken = StringTokenizer(txInput.text.toString(), ";", false)
                edNIM.setText(strToken.nextToken())
                edNam.setText(strToken.nextToken())
                edPro.setText(strToken.nextToken())
            } else {
                Toast.makeText(this, "Dibatalkan", Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        intentIntegrator = IntentIntegrator(this)
        dialog = AlertDialog.Builder(this)
        btnGenerateQR.setOnClickListener(this)
        btnscanQR.setOnClickListener(this)
        btnSave.setOnClickListener(this)
        db = DBOpenHelper(this).writableDatabase
    }


    fun showDataMhs() {
        val cursor: Cursor = db.query(
            "mhs", arrayOf("nim as _id", "nama", "prodi"),
            null, null, null, null, "_id asc"
        )
        lsAdapter =
            SimpleCursorAdapter(
                this, R.layout.item_mahasiswa, cursor,
                arrayOf("_id", "nama", "prodi"), intArrayOf(R.id.txnim, R.id.txnam, R.id.txpro),
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
            ) as ListAdapter
        lv.adapter = lsAdapter

    }

    fun insertDataMhs(nim: String, nama: String, prodi: String) {
        var cv: ContentValues = ContentValues()
        cv.put("nim", nim)
        cv.put("nama", nama)
        cv.put("prodi", prodi)
        db.insert("mhs", null, cv)
        showDataMhs()
    }


    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataMhs(
            edNIM.text.toString(),
            edNam.text.toString(),
            edPro.text.toString()
        )
        edNIM.setText("")
        edNam.setText("")
        edPro.setText("")
        txInput.setText("")
    }
}
